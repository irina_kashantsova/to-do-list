const enterButton = document.getElementById('button');
const input = document.getElementById('input');
const ul = document.querySelector('ul');
const item = document.getElementsByTagName('li');

const inputLength = () => {
    return input.value.length;
};

const listLength = () => {
    return item.length;
};

const createListElement = () => {
    const li = document.createElement('li');
    let div = document.createElement('div');
    const deleteBtn = document.createElement('button');
    const doneBtn = document.createElement('button');

    const crossOut = () => {
        li.classList.toggle('done');
    };

    const doneListItem = () => {
        div.classList.add('task-done');
    };

    const deleteListItem = () => {
        li.remove();
    };

    div.classList.add('task');
    div.innerHTML = input.value;

    li.appendChild(div);
    ul.appendChild(li);
    deleteBtn.appendChild(document.createTextNode('remove'));
    li.appendChild(deleteBtn);
    doneBtn.appendChild(document.createTextNode('done'));
    li.appendChild(doneBtn);

    input.value = '';

    li.addEventListener('click', crossOut);
    deleteBtn.addEventListener('click', deleteListItem);
    doneBtn.addEventListener('click', doneListItem);
};

const addListAfterClick = () => {
    if (inputLength() > 0) {
        createListElement();
    }
}

const addListAfterKeypress = (event) => {
    if (inputLength() > 0 && event.which === 13) {
        createListElement();
    }
}

enterButton.addEventListener('click', addListAfterClick);
input.addEventListener('keypress', addListAfterKeypress);
